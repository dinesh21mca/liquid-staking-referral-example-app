import React from 'react'
import ReactDOM from 'react-dom'
import { Wallet } from './components/wallet/Wallet'
import './css/all.min.css';
import './css/layout.css';

ReactDOM.render(
  <Wallet />,
  document.getElementById('root'),
)
