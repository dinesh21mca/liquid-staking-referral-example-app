import React, { FC, useMemo } from 'react'
import { WalletAdapterNetwork } from '@solana/wallet-adapter-base'
import { ConnectionProvider, WalletProvider } from '@solana/wallet-adapter-react'
import { WalletModalProvider } from '@solana/wallet-adapter-react-ui'
import {
  getLedgerWallet,
  getPhantomWallet,
  getSlopeWallet,
  getSolflareWallet,
  getSolletExtensionWallet,
  getSolletWallet,
  getTorusWallet,
} from '@solana/wallet-adapter-wallets'
import { clusterApiUrl } from '@solana/web3.js'
import { Link } from '@mui/material'
import toast, { Toaster } from 'react-hot-toast'
import { MarinadeProvider } from '../marinade/MarinadeProvider'
import { Panel } from './Panel'
import { Notification, NotificationType } from '../utility/Notification'

import '@solana/wallet-adapter-react-ui/styles.css'
import './Wallet.css'
import Background from '../../images/page-bg.jpg'
import logo from '../../images/logo.png'

import { WalletDisconnectButton, WalletMultiButton } from '@solana/wallet-adapter-react-ui'


const exploreTransactionLink = (network: string, tx: string) => `https://explorer.solana.com/tx/${tx}?cluster=${network}`


export const Wallet: FC = () => {
  const network = WalletAdapterNetwork.Devnet
  const endpoint = useMemo(() => clusterApiUrl(network), [network])

  // @solana/wallet-adapter-wallets imports all the adapters but supports tree shaking --
  // Only the wallets you want to support will be compiled into your application
  const wallets = useMemo(() => [
    getPhantomWallet(),
    getSlopeWallet(),
    getSolflareWallet(),
    getTorusWallet({
      options: { clientId: 'Get a client ID @ https://developer.tor.us' },
    }),
    getLedgerWallet(),
    getSolletWallet({ network }),
    getSolletExtensionWallet({ network }),
  ], [network])

  const onTransaction = (tx: string) => toast.custom(<Notification type={NotificationType.SUCCESS}><Link href={exploreTransactionLink(network, tx)} target='_blank'>{tx}</Link></Notification>)
  const onError = (error: Error) => toast.custom(<Notification type={NotificationType.ERROR}>{error.message}</Notification>)
  

  return <ConnectionProvider endpoint={endpoint}>
    <WalletProvider wallets={wallets} onError={onError} autoConnect>
      <WalletModalProvider>
        <MarinadeProvider>
            <section id="section">
                <section id="page-wrap">
                    <header id="header-wrap">
                        <div className="row d-flex align-center justify-between">
                          <div className="logo">
                            <a href="#">
                              <img alt="" src={logo} />
                            </a>
                          </div>
                          <div className="right-inner">
                            <WalletMultiButton className="head-btn" />
                          </div>
                        </div>
                    </header>
                    <section id="content-wrap">
                        <section className="banner-block" style={{backgroundImage: "url("+Background+")"}}>
                            <div className="main-content">
                                <div className="button">
                                    <a className="btn" href={"https://junglecats.io"}>
                                        <span className="d-flex align-center"><i className="fa-solid fa-angle-left"></i> Return to Emporium</span>
                                    </a>
                                </div>
                                <div className="page-content">
                                    {/*<h1>Stake SOL</h1>
                                    <h6>Via Marinade</h6>
                                    <div className="form-section">
                                        <div className="form-inner d-flex align-center justify-between">
                                            <input type="text" value="" placeholder="0.0" />
                                            <div className="inner-right">SOL</div>
                                        </div>
                                        <div className="center-arrow"><i className="fa-solid fa-arrow-down-long"></i></div>
                                        <div className="form-inner d-flex align-center justify-between">
                                            <input type="text" value="" placeholder="0.0" />
                                            <div className="inner-right">mSOL</div>
                                        </div>
                                        <div className="form-bottom margin-top d-flex align-center justify-between">
                                            <div className="left">APY</div>
                                             <div className="right">6.2%</div>
                                        </div>
                                        <div className="form-bottom d-flex align-center justify-between">
                                            <div className="left">Exchange Rate</div>
                                            <div className="right">1 mSOL = 1.03 SOL</div>
                                        </div>
                                    </div>*/}
                                    <Panel />
                                </div>
                            </div>
                        </section>
                    </section>
                </section>
            </section>
            <Toaster position='bottom-right' reverseOrder={false} />
        </MarinadeProvider>
      </WalletModalProvider>
    </WalletProvider>
  </ConnectionProvider>
}
